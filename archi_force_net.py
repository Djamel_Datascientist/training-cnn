#Copyright Djamel Eddine Hamidi
#Force net cnn aircraft fighter classification

import tensorflow 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense 

Force_Net_Data_set
Force_Net = Sequential()


#convolutional layer with rectified linear unit activation
Force_Net.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
#32 convolution filters used each of size 3x3
#again
Force_Net.add(Conv2D(64, (3, 3), activation='relu'))
#64 convolution filters used each of size 3x3
#choose the best features via pooling
Force_Net.add(MaxPooling2D(pool_size=(2, 2)))
#randomly turn neurons on and off to improve convergence
Force_Net.add(Dropout(0.25))
#flatten since too many dimensions, we only want a classification output
Force_Net.add(Flatten())
#fully connected to get all relevant data
Force_Net.add(Dense(128, activation='relu'))
#one more dropout for convergence' sake :) 
Force_Net.add(Dropout(0.5))
#output a softmax to squash the matrix into output probabilities
Force_Net.add(Dense(num_category, activation='softmax'))